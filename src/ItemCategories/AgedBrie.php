<?php

declare(strict_types=1);

namespace GildedRose\ItemCategories;

class AgedBrie extends NormalItem
{
    public function update(): void
    {
        // Update the quality
        $this->updateQuality();

        // Decrease sell in by 1 each day
        $this->decreaseSellInValueByOne();
    }

    /**
     * Handle quality
     */
    public function updateQuality(): void
    {
        /**
         * If sell in days is negative, increase quality by 2, otherwise by 1
         */
        if (! $this->isWithinSellInDays()) {
            $this->increaseQualityValueByDouble();
            return;
        }
        $this->increaseQualityValueBySingle();
    }
}
