<?php

declare(strict_types=1);

namespace GildedRose\ItemCategories;

class BackstagePasses extends NormalItem
{
    public function update(): void
    {
        // Handle quality
        $this->updateQuality();

        // Decrease sell in by 1 each day
        $this->decreaseSellInValueByOne();
    }

    /**
     * Update quality
     * Is sell in over 10 => increase quality by 1
     * Is sell in over 5 => increase quality by 2
     * Is sell in over 3 => increase quality by 3
     * After the concert the quality is zero
     */
    public function updateQuality(): void
    {
        // Sell in is over 10? add 1 to quality
        if ($this->item->sell_in > 10) {
            $this->increaseQualityValueByValue(1);
            return;
        }

        // Sell in is over 5? add 2 to quality
        if ($this->item->sell_in > 5) {
            $this->increaseQualityValueByValue(2);
            return;
        }

        // Sell in is over 3? add 2 to quality
        if ($this->item->sell_in > 0) {
            $this->increaseQualityValueByValue(3);
            return;
        }

        // Otherwise, it is after the concert, set quality to zero
        $this->setQualityToZero();
    }
}
