<?php

declare(strict_types=1);

namespace GildedRose\ItemCategories;

class Conjured extends NormalItem
{
    /**
     * @var int Quality decreases in 1 day
     */
    protected int $quality_decrease_unit = 2;
}
