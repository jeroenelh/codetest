<?php

declare(strict_types=1);

namespace GildedRose\ItemCategories;

use GildedRose\Item;

class NormalItem
{
    /**
     * @var Item
     */
    protected $item;

    /**
     * @var int Quality decreases in 1 day
     */
    protected int $quality_decrease_unit = 1;

    public function __construct(Item $item)
    {
        $this->item = $item;
    }

    public function update(): Void
    {
        // Update the quality
        $this->updateQuality();

        // Decrease sell in value by one unit
        $this->decreaseSellInValueByOne();
    }

    /**
     * Handle quality property
     */
    public function updateQuality(): Void
    {
        /**
         * If sell in within days is negative, decrease quality by 2, otherwise by 1
         */
        if (! $this->isWithinSellInDays()) {
            $this->decreaseQualityValueByDouble();
            return;
        }
        $this->decreaseQualityValueBySingle();
    }

    /**
     * Is the sell in days above 0
     */
    protected function isWithinSellInDays(): bool
    {
        return $this->item->sell_in > 0;
    }

    /**
     * Decrease Sell In value by 1
     */
    protected function decreaseSellInValueByOne(): void
    {
        $this->item->sell_in--;
    }

    /**
     * Decrease quality value by 1
     */
    protected function decreaseQualityValueBySingle(): void
    {
        $this->item->quality -= $this->quality_decrease_unit;

        if ($this->item->quality < 0) {
            $this->item->quality = 0;
        }
    }

    /**
     * Increase quality value by a single unit
     */
    protected function increaseQualityValueBySingle(): void
    {
        $this->item->quality += $this->quality_decrease_unit;

        if ($this->item->quality > 50) {
            $this->item->quality = 50;
        }
    }

    /**
     * Decrease quality value by a double unit
     */
    protected function decreaseQualityValueByDouble(): void
    {
        $this->item->quality -= ($this->quality_decrease_unit * 2);

        if ($this->item->quality < 0) {
            $this->item->quality = 0;
        }
    }

    /**
     * Increase quality value by a double unit
     */
    protected function increaseQualityValueByDouble(): void
    {
        $this->item->quality += ($this->quality_decrease_unit * 2);

        if ($this->item->quality > 50) {
            $this->item->quality = 50;
        }
    }

    /**
     * Increase quality value by X
     */
    protected function increaseQualityValueByValue(int $value = 1): void
    {
        $this->item->quality += $value;

        if ($this->item->quality > 50) {
            $this->item->quality = 50;
        }
    }

    protected function setQualityToZero(): void
    {
        $this->item->quality = 0;
    }
}
