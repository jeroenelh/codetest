<?php

declare(strict_types=1);

namespace GildedRose;

use GildedRose\ItemCategories\AgedBrie;
use GildedRose\ItemCategories\BackstagePasses;
use GildedRose\ItemCategories\Conjured;
use GildedRose\ItemCategories\NormalItem;
use GildedRose\ItemCategories\Sulfuras;

class ItemFactory
{
    /**
     * Array of non normal items
     * @var array
     */
    protected $specialItems = [];

    public function __construct()
    {
        $this->specialItems['Sulfuras, Hand of Ragnaros'] = Sulfuras::class;
        $this->specialItems['Aged Brie'] = AgedBrie::class;
        $this->specialItems['Backstage passes to a TAFKAL80ETC concert'] = BackstagePasses::class;
        $this->specialItems['Conjured Mana Cake'] = Conjured::class;
    }

    /**
     * Return the item category
     */
    public function getItemCategory(Item $item): NormalItem
    {
        if ($this->isNormalItem($item)) {
            return new NormalItem($item);
        }
        return new $this->specialItems[$item->name]($item);
    }

    /**
     * Determines if the item is standard or special
     */
    public function isNormalItem(Item $item): bool
    {
        return ! key_exists($item->name, $this->specialItems);
    }
}
