<?php

declare(strict_types=1);

namespace Tests;

use GildedRose\GildedRose;
use GildedRose\Item;
use PHPUnit\Framework\TestCase;

class GildedRoseTest extends TestCase
{
    public function testNormalItem(): void
    {
        $items = [new Item('+5 Dexterity Vest', 9, 4)];
        $gildedRose = new GildedRose($items);

        // Test to string method
        $this->assertSame('+5 Dexterity Vest, 9, 4', (string) $items[0]);

        // 1st day
        $gildedRose->updateQuality();
        $this->assertSame(8, $items[0]->sell_in);
        $this->assertSame(3, $items[0]->quality);

        // 2th day
        $gildedRose->updateQuality();
        $this->assertSame(7, $items[0]->sell_in);
        $this->assertSame(2, $items[0]->quality);

        // 3th day
        $gildedRose->updateQuality();
        $this->assertSame(6, $items[0]->sell_in);
        $this->assertSame(1, $items[0]->quality);

        // 4th day
        $gildedRose->updateQuality();
        $this->assertSame(5, $items[0]->sell_in);
        $this->assertSame(0, $items[0]->quality);

        // 5th day
        $gildedRose->updateQuality();
        $this->assertSame(4, $items[0]->sell_in);
        $this->assertSame(0, $items[0]->quality);
    }

    public function testSulfuras(): void
    {
        $items = [new Item('Sulfuras, Hand of Ragnaros', 0, 80)];
        $gildedRose = new GildedRose($items);

        // 1st day
        $gildedRose->updateQuality();
        $this->assertSame(0, $items[0]->sell_in);
        $this->assertSame(80, $items[0]->quality);

        // 2th day
        $gildedRose->updateQuality();
        $this->assertSame(0, $items[0]->sell_in);
        $this->assertSame(80, $items[0]->quality);
    }

    public function testAgedBrie(): void
    {
        $items = [new Item('Aged Brie', 2, 0)];
        $gildedRose = new GildedRose($items);

        // 1st day
        $gildedRose->updateQuality();
        $this->assertSame(1, $items[0]->sell_in);
        $this->assertSame(1, $items[0]->quality);

        // 2th day
        $gildedRose->updateQuality();
        $this->assertSame(0, $items[0]->sell_in);
        $this->assertSame(2, $items[0]->quality);

        // 3th day
        $gildedRose->updateQuality();
        $this->assertSame(-1, $items[0]->sell_in);
        $this->assertSame(4, $items[0]->quality);

        // 4th day
        $gildedRose->updateQuality();
        $this->assertSame(-2, $items[0]->sell_in);
        $this->assertSame(6, $items[0]->quality);

        // 27th day
        for ($i = 0; $i <= 23; $i++) {
            $gildedRose->updateQuality();
        }
        $this->assertSame(-26, $items[0]->sell_in);
        $this->assertSame(50, $items[0]->quality);
    }

    public function testAgedBrie2(): void
    {
        $items = [new Item('Aged Brie', 5, 48)];
        $gildedRose = new GildedRose($items);

        // 1st day
        $gildedRose->updateQuality();
        $this->assertSame(4, $items[0]->sell_in);
        $this->assertSame(49, $items[0]->quality);

        // 2th day
        $gildedRose->updateQuality();
        $this->assertSame(3, $items[0]->sell_in);
        $this->assertSame(50, $items[0]->quality);

        // 3th day
        $gildedRose->updateQuality();
        $this->assertSame(2, $items[0]->sell_in);
        $this->assertSame(50, $items[0]->quality);
    }

    public function testBackstagePasses(): void
    {
        $items = [new Item('Backstage passes to a TAFKAL80ETC concert', 15, 30)];
        $gildedRose = new GildedRose($items);

        // Days left: 14
        $gildedRose->updateQuality();
        $this->assertSame(14, $items[0]->sell_in);
        $this->assertSame(31, $items[0]->quality);

        // Days left: 13
        $gildedRose->updateQuality();
        $this->assertSame(13, $items[0]->sell_in);
        $this->assertSame(32, $items[0]->quality);

        // Days left: 10
        $gildedRose->updateQuality();
        $gildedRose->updateQuality();
        $gildedRose->updateQuality();
        $this->assertSame(10, $items[0]->sell_in);
        $this->assertSame(35, $items[0]->quality);

        // Days left: 9
        $gildedRose->updateQuality();
        $this->assertSame(9, $items[0]->sell_in);
        $this->assertSame(37, $items[0]->quality);

        // Days left: 5
        $gildedRose->updateQuality();
        $gildedRose->updateQuality();
        $gildedRose->updateQuality();
        $gildedRose->updateQuality();
        $this->assertSame(5, $items[0]->sell_in);
        $this->assertSame(45, $items[0]->quality);

        // Days left: 4
        $gildedRose->updateQuality();
        $this->assertSame(4, $items[0]->sell_in);
        $this->assertSame(48, $items[0]->quality);

        // Days left: 2
        $gildedRose->updateQuality();
        $gildedRose->updateQuality();
        $this->assertSame(2, $items[0]->sell_in);
        $this->assertSame(50, $items[0]->quality);

        // Days left: 0
        $gildedRose->updateQuality();
        $gildedRose->updateQuality();
        $this->assertSame(0, $items[0]->sell_in);
        $this->assertSame(50, $items[0]->quality);

        // After the event
        $gildedRose->updateQuality();
        $this->assertSame(-1, $items[0]->sell_in);
        $this->assertSame(0, $items[0]->quality);
    }

    public function testConjured(): void
    {
        $items = [new Item('Conjured Mana Cake', 3, 6)];
        $gildedRose = new GildedRose($items);

        // 1st day
        $gildedRose->updateQuality();
        $this->assertSame(2, $items[0]->sell_in);
        $this->assertSame(4, $items[0]->quality);

        // 2th day
        $gildedRose->updateQuality();
        $this->assertSame(1, $items[0]->sell_in);
        $this->assertSame(2, $items[0]->quality);

        // 3th day
        $gildedRose->updateQuality();
        $this->assertSame(0, $items[0]->sell_in);
        $this->assertSame(0, $items[0]->quality);

        // 4th day
        $gildedRose->updateQuality();
        $this->assertSame(-1, $items[0]->sell_in);
        $this->assertSame(0, $items[0]->quality);
    }
}
